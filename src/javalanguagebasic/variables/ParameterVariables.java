package javalanguagebasic.variables;

public class ParameterVariables {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 10;
		change_voidMethod(num);// no change because num is local variable when use void method
		System.out.println(num);		
		
		//when use return method so return right type of method
		System.out.println(change_returnMethod(num));
	}

	private static void change_voidMethod(int param) {
		// TODO Auto-generated method stub
		param = 20;
	}
	
	private static int change_returnMethod(int arg) { //arg is parameter variable (arg = num)
		// TODO Auto-generated method stub
		arg = 20;
		return arg;
	}

}
