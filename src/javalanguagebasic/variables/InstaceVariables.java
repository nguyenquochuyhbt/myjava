package javalanguagebasic.variables;

public class InstaceVariables {

	private int instance;// is one model in class so must declare class(new) to use variable
	
	public InstaceVariables() {//default constructor to create instance variable => defalt value int = 0
		instance = 0;
	}
	
	public InstaceVariables(int instance) {

		this.instance = instance;
	}

	public int getInstance() {
		return instance;
	}

	public void setInstance(int instance) {
		this.instance = instance;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(instance); no use => non-static variable
		
		InstaceVariables insVar = new InstaceVariables();
		System.out.println(insVar.getInstance());
		
		//if set different value to instance
		insVar.setInstance(90);
		System.out.println(insVar.getInstance());
	}

}
