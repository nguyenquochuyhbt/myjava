package javalanguagebasic.variables;

public class MethodOfIntegerVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n1 = 10; // decimal value
		int n2 = 017;// octal value(8 based) value 0-8 = (0*8)^0 + (1*8)^1 + (0*8)^2
		int n3 = 0x20F;// hexa value(16 based) value 0-9 A-F = (0*16)^0 + (1*16)^1 + (0*16)^2
		int n4 = 0b111;// binary value(2 based) value 0-1= (0*2)^0 + (1*2)^1 + (0*2)^2 + (1*2)^2 	
		
		// screen(console) 
		System.out.println( n1 );// call function
		System.out.println( n2 );// call function
		System.out.println( n3 );// call function
		System.out.println( n4 );// call function
		
		// decimal value in console
		System.out.println( n1 );// call function
		System.out.println( Integer.toOctalString(n2) );// call function change octal -> decimal
		System.out.println( Integer.toHexString(n3) );// call function
		System.out.println( Integer.toBinaryString(n4) );// call function
		// use Library in Java API
		// english word
	}

}
