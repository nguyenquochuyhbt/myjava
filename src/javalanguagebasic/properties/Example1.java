package javalanguagebasic.properties;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;

public class Example1 {

	private static final String FILE_CONFIG = "\\db.properties";
	
	public static void main(String[] args) {
		String[] arr = LoadProperties();
		
		writeArticle(arr[0]);
		readArticle(arr[0]);
	}
	
	private static File checkFileArticle(String path) {
		
		File file = new File(path);
		
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return file;
	}
	
	private static void writeArticle(String path) {
		 File filepath = checkFileArticle(path);
		 
	}

	private static void readArticle(String path) {
		File filepath = checkFileArticle(path);
		
		Article article = null;
		
	}

	public static String[] LoadProperties(){
		
		String[] arr = new String[2];
		
		Properties props = new Properties();
		FileInputStream fis = null;
	
		try {
			fis = new FileInputStream("config1/" + FILE_CONFIG);
			
			props.load(fis);
			
			if(props.getProperty("article") != null) {
				arr[0] = props.getProperty("article");
			}
			if(props.getProperty("blogger") != null) {
				arr[1] = props.getProperty("blogger");
			}
			System.out.println(props.getProperty("blogger"));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return arr;
	}

}
