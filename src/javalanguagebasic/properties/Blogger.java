package javalanguagebasic.properties;

import java.io.Serializable;

public class Blogger{
	private Long uid;
	private String name;// show in screen
	private String email;
	private String passwd;
	
	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	@Override
	public String toString() {
//		System.out.println(super.toString());
		return "Blogger [uid=" + uid + ", name=" + name + ", email=" + email + ", passwd=" + passwd + "]";
	}

}

