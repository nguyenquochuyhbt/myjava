package javalanguagebasic.properties;

import java.io.Serializable;
import java.util.Date;

// blog contents Modeling and Relation <- R DBMS
public class Article implements Serializable{
	    private Long aid;
	    private String title;
	    private String content;
	    private String name;
	    private Date wdate;

	    public Long getAid() {
	        return aid;
	    }
	    public void setAid(Long aid) {
	        this.aid = aid;
	    }
	    public String getTitle() {
	        return title;
	    }
	    public void setTitle(String title) {
	        this.title = title;
	    }
	    public String getContent() {
	        return content;
	    }
	    public void setContent(String content) {
	        this.content = content;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public Date getWdate() {
	        return wdate;
	    }

	    public void setWdate(Date wdate) {
	        this.wdate = wdate;
	    }

	    // Article ëª¨ë�¸ Copy
	    public void CopyData(Article param)
	    {
	        this.aid = param.getAid();
	        this.title = param.getTitle();
	        this.content = param.getContent();
	        this.name = param.getName();
	        this.wdate = param.getWdate();
	    }
		public Article(Long aid, String title, String content, String name, Date wdate) {
			this.aid = aid;
			this.title = title;
			this.content = content;
			this.name = name;
			this.wdate = wdate;
		}
		public Article() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public String toString() {
			return "Article [aid=" + aid + ", title=" + title + ", content=" + content + ", name=" + name + ", wdate="
					+ wdate + "]";
		}
		
	    
}
