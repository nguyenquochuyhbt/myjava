package javalanguagebasic.lambda;


@FunctionalInterface
public interface ListString<T> {
	public void Output(T t);
}
