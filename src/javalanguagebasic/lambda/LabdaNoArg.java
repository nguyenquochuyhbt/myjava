package javalanguagebasic.lambda;

public class LabdaNoArg {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SaySomething s = () -> {
			System.out.println("Say hi everybody");
		};
		
		s.Say();
	}

}
