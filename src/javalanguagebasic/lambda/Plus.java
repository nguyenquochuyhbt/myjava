package javalanguagebasic.lambda;

public class Plus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculator c = (a, b) -> {
			return a + b;
		};
		System.out.println(c.Plus(3, 4));
		
		Calculator d = (a,b) -> (a + b);
		System.out.println(d.Plus(2, 3));
	}

}
