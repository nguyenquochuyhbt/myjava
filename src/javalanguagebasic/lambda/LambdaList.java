package javalanguagebasic.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class LambdaList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> abc = new ArrayList<String>();
		abc.add("huy");
		abc.add("dat");
		abc.add("khoi");
		abc.add("minh");
		
		Consumer<? super String> arr = (item) -> System.out.println(item);
		abc.forEach(arr);
		
		
		Consumer arr1 = (item) -> System.out.println(item);	
		abc.forEach(arr1);
		
		Consumer<String> arr2 = (item) -> System.out.println(item);
		abc.forEach(arr2);// abc --> arr2 --> out
		
	}

}
