package javalanguagebasic.lambda;

/* cung cap chuc nang cho interface (chi co mot phuong thuc truu tuong trong interface) => Functional Interface
 * 
 * labda form: (argument-list) -> {body}
 * argument-list : can use ignore
 * arrow-token -> : use to connect argument-list and body
 * body: include bieu thuc va cau lenh cho bieu thuc lambda
 */




public class NoUseLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int width = 10;
		
		Drawable d = new Drawable(){
			@Override
			public void draw() {
				System.out.println("Drawing " + width);
			}
		};
		
		d.draw();
	}

}
