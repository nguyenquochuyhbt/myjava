package javalanguagebasic.fileio.input;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Writefile01 {

	static int num;
	
	static String str;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		write01();
	}

	private static void write01() {
		// TODO Auto-generated method stub
		num = 23;
		str = "huy";
		File file = new File("data/create01.txt");
		try {
			FileOutputStream fos = new FileOutputStream(file);// use int and byte[]
			fos.write(Integer.toString(num).getBytes());// not save int value
			fos.write(str.getBytes());//must use method getByte
			fos.flush();
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(file.exists()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.open(file.getAbsoluteFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
