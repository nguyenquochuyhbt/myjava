package javalanguagebasic.fileio.input;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

public class Writefile03 implements Serializable {

	static ArrayList<Object> models;
	
	private int id;
	
	private String name;
	
	private String content;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", name=" + name + ", content=" + content + "]";
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		models = new ArrayList<Object>();
		Writefile03 n1 = new Writefile03();
		n1.setId(1);
		n1.setName("huy");
		n1.setContent("UIT");
		Writefile03 n2 = new Writefile03();
		n2.setId(2);
		n2.setName("dat");
		n2.setContent("UIT");
		models.add(n1);
		models.add(n2);
		
		write03();
	}

	
	public static void write03() {
		// append
		File file = new File("data","create03.txt");
		
		try {
			PrintWriter writer = new PrintWriter(file);
			for(Object model : models) {
				writer.append(model.toString()+"\n");
				writer.flush();
			}
			writer.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		if(file.exists()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.open(file.getAbsoluteFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	

}
