package javalanguagebasic.fileio.input;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Writefile02 {

	static Map<Integer, String> map;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		write02();
	}

	private static void write02() {
		// TODO Auto-generated method stub
		map = new HashMap<Integer, String>();
		map.put(1, "huy");
		map.put(2, "hoang");
		
		File file = new File("data/create02.txt");
				
			try {
				FileOutputStream fos = new FileOutputStream(file);
				for(Entry<Integer, String> entry: map.entrySet()) {
					byte[] id = Integer.toString(entry.getKey()).getBytes();
					byte[] name = entry.getValue().getBytes();
					fos.write(id);
					fos.write(name);
					fos.flush();
				}
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		if(file.exists()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.open(file.getAbsoluteFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
