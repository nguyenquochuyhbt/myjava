package javalanguagebasic.fileio.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Writefile04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		write04();
	}

	private static void write04() {
		// TODO Auto-generated method stub
		Writefile03 n1 = new Writefile03();
		n1.setId(3);
		n1.setName("minh");
		n1.setContent("UIT");
		
		File file = new File("data/create03.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		
		try {
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(n1);// must n1 implements Serializable
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(fos != null) {
					fos.close();
				}
				if(oos != null) {
					oos.close();
				}
			}catch(IOException e) {
				e.printStackTrace();
			}	
		}
	}

}
