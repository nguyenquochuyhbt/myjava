package javalanguagebasic.fileio.file;

import java.io.File;
import java.io.IOException;

public class Createfile02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		create02();
	}

	private static void create02() {
		// TODO Auto-generated method stub
		File file = new File("data", "create02.txt");
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(file.getAbsolutePath());
		
		file = new File("data", "create02");
		if(file.exists()) {
			file.delete();
			System.out.println("Delete success");
		}
	}
	
	

}
