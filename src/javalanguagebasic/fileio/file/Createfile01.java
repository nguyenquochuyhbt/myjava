package javalanguagebasic.fileio.file;

import java.io.File;
import java.io.IOException;

public class Createfile01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		create01();
	}

	private static void create01() {
		// TODO Auto-generated method stub
		File dir = new File("data");//folder data01
		
		if(!dir.exists()) {
			dir.mkdir();// check exists folder
			System.out.println(dir.isDirectory());
		}else {
			System.out.println(dir.getAbsolutePath());
		}
		
		File file = new File("data/create01.txt");
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(file.exists());
		System.out.println(file.getAbsolutePath());
		
		File file2 = new File("../create02.txt");//out project
		try {
			file2.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(file2.getAbsolutePath());
	}

}
