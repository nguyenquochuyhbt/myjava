package javalanguagebasic.fileio.file;

import java.io.File;

public class File02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		file02();
	}

	private static void file02() {
		// TODO Auto-generated method stub
		File file = new File("data/test01.txt");
		if( file.exists() ) {
			file.delete();
		}
		
		// read, write in file, file has information 
		// use Stream
		System.out.println(file.canExecute());
		System.out.println(file.canRead());
		System.out.println(file.canWrite());
		file.setExecutable(false);
		System.out.println(file.canExecute());
		
		System.out.println(file.isDirectory());
		System.out.println(file.isFile());
		System.out.println(file.isHidden());
	}
	
	

}
