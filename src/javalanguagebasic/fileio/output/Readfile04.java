package javalanguagebasic.fileio.output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Readfile04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		read04();
	}

	private static void read04() {
		// TODO Auto-generated method stub
		File file = new File("data/create03.txt");
		if(file.exists()) {
			FileInputStream fis = null;
			ObjectInputStream ois = null;
			
			try {
				fis = new FileInputStream(file);
				ois = new ObjectInputStream(fis);
				Object obj = new Readfile03();
				//Readfile03 n1 = (Readfile03) obj.readObject();// must n1 implements Serializable
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if(fis != null) {
						fis.close();
					}
					if(ois != null) {
						ois.close();
					}
				}catch(IOException e) {
					e.printStackTrace();
				}	
			}
		}
	}

}
