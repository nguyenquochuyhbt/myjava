package javalanguagebasic.fileio.output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

public class Readfile03 implements Serializable{
	
	static ArrayList<Readfile03> models;
		
	private Long id;
	
	private String name;
	
	private String content;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", name=" + name + ", content=" + content + "]";
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		models = new ArrayList<>();
		read03();
		for(Readfile03 e: models) {
			System.out.println("[id=" + e.getId() + ", name=" + e.getName() + ", content=" + e.getContent() + "]");
		}
	}

	private static void read03() {
		// TODO Auto-generated method stub
		File file = new File("data", "create03.txt");
		
		try {
			if(file.exists()) {
				Scanner sc = new Scanner(file);
				boolean check = false;
				Readfile03 model = new Readfile03();
				while(sc.hasNext()) {	
					String ct = sc.nextLine();
					int begin = ct.indexOf('[');
					int end = ct.indexOf(']');
					String sub = ct.substring(begin + 1, end);
					String[] split1 = sub.split(",");
					
					for(String e : split1) {
						String[] split2 = e.trim().split("=");
						
						switch(split2[0]) {
							case "id":
								model.setId(Long.parseLong(split2[1]));
								break;
							case "name":
								model.setName(split2[1]);
								break;
							case "content":
								model.setContent(split2[1]);
								check = true;
								break;
						}
						
						if(check) {
							models.add(model);
							check = false;
							model = new Readfile03();
						}
						
					}

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
