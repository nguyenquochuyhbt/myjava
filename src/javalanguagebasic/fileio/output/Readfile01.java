package javalanguagebasic.fileio.output;

import java.io.File;
import java.io.FileInputStream;

public class Readfile01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		read01();
	}

	private static void read01() {
		// TODO Auto-generated method stub
		File file = new File("data","create03.txt");
		
		try {
			if(file.exists()) {
				System.out.println(file.length()+" bytes");
			}
			FileInputStream fis = new FileInputStream(file);
			
			int data;
			StringBuilder builder = new StringBuilder("DATA\n");
			while((data = fis.read()) != -1) {
				builder.append((char)data);// int -> char
			}
			System.out.println(builder.toString());
			
//			int data1 = fis.read();
//			while((data1 = fis.read()) != -1) {
//				System.out.println(data);
//			}// no int
			
//			byte[] buff = new byte[(int)file.length()];//check < Integer.MAX
//			fis.read(buff);
//			for(int i=0; i<buff.length ; i++) {
//				System.out.println(buff[i]);
//			} no byte

			fis.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	

}
