package javalanguagebasic.fileio.output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Readfile02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		read02();
	}

	private static void read02() {
		// TODO Auto-generated method stub
		File file = new File("data", "create03.txt");
				
		try {
			FileInputStream fis = new FileInputStream(file);
			if(file.exists()) {
				int data;
				StringBuilder builder = new StringBuilder();
				while((data = fis.read()) != -1) {
					builder.append((char)data);
				}
				System.out.println(builder);
				
				Scanner sc = new Scanner(file);
				while(sc.hasNext()) {
					System.out.println(sc.nextLine());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}	

}
