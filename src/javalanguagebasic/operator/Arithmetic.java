package javalanguagebasic.operator;

public class Arithmetic {

	static double a;
	static double b;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		a = 1;
		b = 2;
		
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		System.out.println(a/b);
		System.out.println(a%b);
		
		a++;// same a += 1 same a = a + 1 
		System.out.println(a++);// a will implement before up
		
		double rs = a-- + b; // a will implement before down
		System.out.println(rs);
		System.out.println(a);
		
		System.out.println(++a); // a will up before implement
		
	}

}
