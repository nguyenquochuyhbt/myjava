package javalanguagebasic.operator;

public class Bit {

	static int num = 5;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// ~ & | ^ >> <<
		
		// a = 5 (decimal) = 0000 0101 = from right to left = 1x2^0 + 0x2^1 + 1x2^2 + 0x2^3 = 5
		// ~0000 0101 = 1111 1010 + 1 = 1101 = 0x2^0 + 1x2^1 + 0x2^2 + 1x2^3 = 10
		System.out.println(~num);
	}

}
