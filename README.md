**THE JAVA PROGRAMMING LANGUAGE**
 
 The Java programming language is a high-level language that can be characterized by all of the following buzzwords:

 .   Simple

 .   Object oriented

 .   Distributed

 .   Multithreded

 .   Dynamic

 .   Architecture neutral

 .   Portable

 .   High performance

 .   Robust
 
 .   Secure

 Each of the preceding buzzwords is explained in The Java Language Enviroment, a white paper written by James Gosling and Henry McGilton.

 Link pdf: https://www.stroustrup.com/1995_Java_whitepaper.pdf

 In the Java programming language, all source code is first written in plain text files ending with the .java extension. Those source files are then complied into .class files by the javac compiler. A .class file does not contain code that is native to tyour processor; it instead contains bytecodes - the machine language of the Java Virtual Machine (Java VM). The java launcher tool then runs your application with an instance of the Java Virtual Machine.

 An overview of the software development process:

MyProgram.java  ----- Compiler ----->   MyProgram.class ----- Java VM ----- 01000101... ----->  My Program

 Because the Java VM is available on many different operating systems, the same .class files are capable of running on Microsoft Windows, the Solaris Operating System (Solaris OS), Linux, or Mac OS. Some virtual machines, such as the Java SE HotSpot at a Glance, perform additional steps at runtime to give your application a performace boost. This includes various tasks such as finding performance bottlenecks and recompiling (to native code) frequently used sections of code.

 An overview Throught the Java VM, the same application is capable of running on multiple plattforms:

                                Java Program (.java)
                                            
                                    Complier

                    JVM                 JVM                 JVM
            
                    Win32               Unix               MacOS   


**THE JAVA PLATTFORM**

 A plattform is the hardware or software environment in which a program runs. We've already mentioned some of the most popular platforms like Microsoft Windows, Linux, Solaries OS, and Mac OS. Most plattforms can be describes as a combination of the operating system and underlying hardware. The Java platform differs from most other platforms in that it's a software-only platform that runs on top of other hardware-based platforms.

 The Java platform has two components:
 .  The Java Virtual Machine
 .  The Java Application Programming Interface (API)

 You've already been introduced to the Java Virtual Machine; it's the base for the Java platform and is ported onto various hardware-based platforms.

 The API is a large collection of ready-made software components that provide many useful capabilties. It is grouped into libraries of related classes and interfaces; these libraries are known as packages. The next section, What Can Java Technology Do? highlights some of the functionality provided by the API.

                    MyProgram.java
    API                     
                                =>Java platform
    Java Virtual Machine

    Hardware-Base Plattform

 The API and Java Virtual Machine insulate the program from the underlying hardware.

 As a platform-independent environment, the Java platform can be a bit slower than native code. However, advances in compiler and virtual machine technologies are bringing performance close to that of native code without threatening portability.

 The terms "Java Virtual Machine" and "JVM" mean a Virual Machine for the Java platform.

 The general-purpose, high-level Java programming language is a powerful software platform. Every full implementation of the Java platform gives you the following features:

 .  Development Tools: The development tools provide everything you'll need for compiling, running, monitoring, debigging, and documenting your applications. As a launcher, and the javadoc documentation tool.

 .  Application Programming Inteface (API): The API provides the core functionality of the Java programming language. It offers a wide array of useful classes ready for use in your own applications. Its spans everything from bases objects, to networking and security, to XML generation and databases access, and more. The core API is very large; to get an overview of what it contains, consult the Java Platform Standard Edition 8 Documentation.

 .  Deployment Technologies: The JDK software provides standard mechanisms such as the Java Web Start software and Java Plug-In software for deploying your applications to end users.

 .  User Interface Toolkits: The JavaFX, Swing, and Java 2D toolkits make it possible to create sophisticated Graphical User Interfaces (GUIs).
 
 .  Integration Libraries: Integration libraries such as the Java IDL API, JDBC API, Java Naming and Directory Interface (JNDI) API, Java RMI, and Java Remote Method Invocation over Internet Inter-ORB Protocal Technology (Java RMI-IIOP Technology) enable database access and manupulation of remote objects.
